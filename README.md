# exchange server

### Тестовое задание

### Start app:
1. `make build`
2. `docker-compose up -d`

### Environment variables:
- `PG_USER`
- `PG_PASS`
- `PG_HOST`
- `PG_PORT`
- `PG_DBNAME`
- `SERVER_PORT`

### Command line flags:
- `pguser`
- `pghost`
- `pgport`
- `dbname`
- `port`

### Run tests:
- `make test`
