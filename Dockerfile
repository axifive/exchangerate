FROM golang:1.22.3-alpine3.20

WORKDIR /usr/src/app

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -v -o /usr/local/bin/exchangeserver ./

EXPOSE 50051

CMD ["exchangeserver"]