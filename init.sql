CREATE TABLE public.rates (
    id serial PRIMARY KEY,
    ask decimal(12,2) NOT NULL,
    bid decimal(12,2) NOT NULL,
    date timestamp with time zone DEFAULT now() NOT NULL
)
