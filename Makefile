.PHONY: build
build:
	@rm -rf ./out
	@mkdir out
	@go build -v -o ./out/exchangeserver ./

.PHONY: test
test:
	@go test -cover -v ./...

.PHONY: run
run:
	@go run ./

.PHONY: lint
lint:
	@golangci-lint run ./... -v

.PHONY: docker-build
docker-build:
	@docker build --tag exchangeserver:master .