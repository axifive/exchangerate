package config

import (
	"os"
	"testing"
)

var (
	pguser = "postgres"
	pgpass = "1234"
	pghost = "localhost"
	pgport = "5432"
	dbname = "test"
	port   = "50051"
)

func TestGetConfigFromArgs(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	os.Args = []string{"test", "-pguser=" + pguser, "-pgpass=" + pgpass, "-pghost=" + pghost, "-pgport=" + pgport, "-dbname=" + dbname, "-port=" + port}
	config := GetConfig()
	if config.PgUser != pguser {
		t.Errorf("incorrect pguser")
	}
	if config.PgPass != pgpass {
		t.Errorf("incorrect pgpass")
	}
	if config.PgHost != pghost {
		t.Errorf("incorrect pghost")
	}
	if config.PgPort != pgport {
		t.Errorf("incorrect pgport")
	}
	if config.PgDbName != dbname {
		t.Errorf("incorrect dbname")
	}
	if config.ServerPort != port {
		t.Errorf("incorrect port")
	}
}
