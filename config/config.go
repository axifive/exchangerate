package config

import (
	"flag"
	"os"
)

type Config struct {
	PgUser     string
	PgPass     string
	PgHost     string
	PgPort     string
	PgDbName   string
	ServerPort string
}

var config *Config

func envOrDefault(envVal, defaultVal string) string {
	if len(envVal) != 0 {
		return envVal
	}
	return defaultVal
}

// GetConfig return config struct with value from Environment or passed as flags, flags have higher priority
func GetConfig() *Config {
	if config == nil {
		config = &Config{
			envOrDefault(os.Getenv("PG_USER"), "postgres"),
			envOrDefault(os.Getenv("PG_PASS"), "1234"),
			envOrDefault(os.Getenv("PG_HOST"), "localhost"),
			envOrDefault(os.Getenv("PG_PORT"), "5432"),
			envOrDefault(os.Getenv("PG_DBNAME"), "test"),
			envOrDefault(os.Getenv("SERVER_PORT"), "50051"),
		}
		config.PgUser = *flag.String("pguser", config.PgUser, "postgres username")
		config.PgPass = *flag.String("pgpass", config.PgPass, "postgres password")
		config.PgHost = *flag.String("pghost", config.PgHost, "postgres ip or hostname")
		config.PgPort = *flag.String("pgport", config.PgPort, "postgres port")
		config.PgDbName = *flag.String("dbname", config.PgDbName, "postgres db name")
		config.ServerPort = *flag.String("port", config.ServerPort, "server port")
		flag.Parse()
	}
	return config
}
