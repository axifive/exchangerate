package db

import (
	"context"
	"exchangerate/config"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"time"
)

type DB struct {
	connPool *pgxpool.Pool
}

type AskBid struct {
	Ask       string
	Bid       string
	Timestamp int64
}

var db *DB

func GetDBConnection(config *config.Config) (*DB, error) {
	if db == nil {
		pgUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", config.PgUser, config.PgPass, config.PgHost, config.PgPort, config.PgDbName)
		connPool, err := pgxpool.New(context.Background(), pgUrl)
		if err != nil {
			return nil, err
		}
		err = connPool.Ping(context.Background())
		if err != nil {
			return nil, err
		}
		db = &DB{connPool: connPool}
	}
	return db, nil
}

func (db *DB) Close() {
	db.connPool.Close()
}

func (db *DB) InsertRate(ask, bid string, timestamp time.Time) error {
	_, err := db.connPool.Exec(context.Background(), "INSERT INTO rates(ask, bid, date) values ($1, $2, $3)", ask, bid, timestamp)
	return err
}

func (db *DB) GetLastRate() (*AskBid, error) {
	askBid := &AskBid{}
	var timestamp time.Time
	err := db.connPool.QueryRow(context.Background(), "SELECT ask, bid, date FROM rates ORDER BY date DESC LIMIT 1").Scan(&askBid.Ask, &askBid.Bid, &timestamp)
	askBid.Timestamp = timestamp.Unix()
	return askBid, err
}
