package garantexclient

import (
	"encoding/json"
	"exchangerate/db"
	"io"
	"log"
	"net/http"
	"time"
)

const API_URL = "https://garantex.org/api/v2/depth?market=usdtrub"

type Offer struct {
	Price  string `json:"price"`
	Volume string `json:"volume"`
	Amount string `json:"amount"`
	Factor string `json:"factor"`
	Type   string `json:"type"`
}

type Depth struct {
	Timestamp int64   `json:"timestamp"`
	Asks      []Offer `json:"asks"`
	Bids      []Offer `json:"bids"`
}

var client *http.Client

func getClient() *http.Client {
	if client == nil {
		client = &http.Client{
			Timeout: time.Second * 2,
		}

		//client = &http.Client{}
	}
	return client
}

/*
func GetDepthFake() (*db.AskBid, error) {
	return &db.AskBid{
		Ask:       "1",
		Bid:       "1",
		Timestamp: time.Now().Unix(),
	}, nil
}*/

func GetDepth() (*db.AskBid, error) {
	client := getClient()
	req, err := http.NewRequest("GET", API_URL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	depth := &Depth{}
	err = json.Unmarshal(body, depth)
	if err != nil {
		return nil, err
	}
	if len(depth.Asks) == 0 || len(depth.Bids) == 0 {
		return nil, err
	}
	askBid := &db.AskBid{
		Ask:       depth.Asks[0].Price,
		Bid:       depth.Bids[0].Price,
		Timestamp: depth.Timestamp,
	}
	return askBid, nil
}
