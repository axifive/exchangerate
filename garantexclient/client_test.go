package garantexclient

import (
	"testing"
)

func TestGetDepth(t *testing.T) {
	askBid, err := GetDepth()
	if err != nil {
		t.Errorf("Error when try get depth from api endpoint")
	}
	if askBid == nil || len(askBid.Ask) == 0 || len(askBid.Bid) == 0 || askBid.Timestamp == 0 {
		t.Errorf("Error when try get depth from api endpoint")
	}
}
