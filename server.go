package main

import (
	"context"
	"exchangerate/config"
	"exchangerate/db"
	"exchangerate/garantexclient"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"

	pb "exchangerate/protobuf"
)

var (
	conf   *config.Config
	dbconn *db.DB
)

type exchangeServer struct {
	pb.UnimplementedRateServiceServer
}

// GetRates rpc method
func (s *exchangeServer) GetRates(ctx context.Context, empty *pb.Empty) (*pb.Rate, error) {
	log.Println("Requested current Rate")
	askBid, err := garantexclient.GetDepth()
	if err != nil {
		askBid, err = dbconn.GetLastRate()
		if err != nil {
			return nil, err
		}
	} else {
		err = dbconn.InsertRate(askBid.Ask, askBid.Bid, time.Unix(askBid.Timestamp, 0))
		if err != nil {
			log.Println(err)
			return nil, err
		}
	}
	return &pb.Rate{Ask: askBid.Ask, Bid: askBid.Bid, Timestamp: askBid.Timestamp}, nil
}

func main() {
	conf = config.GetConfig()

	var err error
	dbconn, err = db.GetDBConnection(conf)
	if err != nil {
		log.Fatalf("failed to create db connection: %v", err)
	}
	defer dbconn.Close()

	listen, err := net.Listen("tcp", fmt.Sprintf(":%s", conf.ServerPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterRateServiceServer(s, &exchangeServer{})
	log.Printf("server listening at %v", listen.Addr())
	if err := s.Serve(listen); err != nil {
		log.Fatalf("failed to serve pb: %v", err)
	}
}
